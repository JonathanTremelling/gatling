Gatling Presentation
==================

Headers
--------------

- Key, value pairs
- Can also be stored as a Map
- Included using the keyword ".header()" or ".headers()"
- Can include triple quotes to avoid escaping characters

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/f7d73a151282cd59502211765dbd1e18676b5600/doc/headers.png?token=3c15a95c346fb08a323764025a492b4dcd00ff2f)
	
[Objects](objects.md) |:-:| [Checks,Transforms & Storing Parameters](checksTransformsAndStoringParameters.md)


