Gatling Presentation
==================

What is Gatling?
--------------

- Load testing framework
- Lightweight & easy to deploy
- Scalable
- Easy to use
- Provides user-friendly output in the form of HTML


What can we use it for?
------------------------

- Used for automated testing for performance and functionality checks on your code
- Written in Scala
- Good for use with different test scenarious and setups
	- E.g. Admin endpoints Vs. User endpoints
- Integrates With Jenkins
    
[Overview](../README.md) |:-:| [HTTP Configuration](httpConfiguration.md)