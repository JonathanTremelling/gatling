Gatling Presentation
==================

Additional Setup Parameters
--------------

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/e986a83ca0927b0815068f84eaa219e3888af38e/doc/additionalSettings.png?token=7e024b75f874de969d5d8c2890b97d0f0d9ebdd4)


![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/e986a83ca0927b0815068f84eaa219e3888af38e/doc/additionalSettings2.png?token=72dec38afca3c38092ef539af9f7432ecc18f930)

	
[Maven And Jenkins](mavenAndJenkins.md) |:-:| [Other Feed Types](otherFeedTypes.md)


