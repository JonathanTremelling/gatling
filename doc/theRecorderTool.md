Gatling Presentation
==================

The Recorder Tool
--------------

- Help writing scenarios, no need to do it from scratch
- Can connect directly to your server
- Records actions on a web application
- Can configure request using GUI
- Save output responses
- Can specify which files are ignored
- http://gatling.io/docs/1.5.6/user_documentation/reference/recorder.html

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/b330cc78158d5db0e35d8da081dff6d71c44c951/doc/recorderTool.png?token=744814361e3414719716ef784da7a82266a1ad40)

	
[Other Feed Types](otherFeedTypes.md) |:-:| [Gatling Resources](gatlingResources.md)


