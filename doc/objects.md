Gatling Presentation
==================

Objects
--------------

- Encompass test executions
- Tests can be stored in variables
- Variables can be accessed specifically through objects

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/b4718d20278c92e6fa064255cfc542873718b587/doc/object.PNG?token=de52a58841e5c3b4b3e0957445eb5a3a0604ec5a)

- Can perform multiple tests inside one object
- Easily separate logic and functions
- Creates separate results HTML page per object variable
- See LoginTestAsObject example.

[Scenarios](scenarios.md) |:-:| [Gatling Headers](headers.md)