Gatling Presentation
==================

Looping & Sessions
--------------

- http://stackoverflow.com/questions/25289334/gatling-looping-through-json-array

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/d5c96612f9d372d807cb6395bf1f109fbbe22ca2/doc/foreach.PNG?token=4fbbf5a7d6d7fc1ecfc44557be180dd9d1271343)
	
- See ForeachTest example.
	
[Reading A CSV File](readingACsvFile.md) |:-:| [Maven And Jenkins](mavenAndJenkins.md)


