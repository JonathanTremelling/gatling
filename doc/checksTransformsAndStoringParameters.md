Gatling Presentation
==================

Checks,Transforms & Storing Parameters
--------------

- Check HTTP response codes
- Save parts of JSON
- Split string into array


![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/e4fd43294a4c2acf8bc1372defd4e8d8e2a6605c/doc/transforms.PNG?token=246d014ab20ed4b9a9bd136cb4a23455265aee46)
	
[Gatling Headers](headers.md) |:-:| [Reading A CSV File](readingACsvFile.md)


