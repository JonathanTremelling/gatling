Gatling Presentation
==================

Scenarios
--------------

- Used to call specific objects and tests
- Can be tailored to different testers and environments
- Can be run with a number of users
- Users can be started all at once or gradually
	- atOnceUsers(1000) // To start all users at the same time
	- rampUsers(1000) over (30 seconds) // To start gradually, over 30 seconds

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/496959502f455318f638ff6336fa1ab4612c6032/doc/scenario.PNG?token=e755e8dd983d1c6e80c47f67d5e28f435d3df929)
	

Basic Login Test
-------------------

To run a Gatling test in the command line:

- "sbt"
	- Test plugin
- "test"
	- Runs all tests
- "testOnly npower.BasicLoginTest"
	- Runs BasicLoginTest
- HTML output returned
	
[HTTP Configuration](httpConfiguration.md) |:-:| [Objects](objects.md)