Gatling Presentation
==================

Reading A CSV File
--------------

- Saves a lot of code and time
- Easy to update / maintain
- Keeps things simple and code easy to read

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/5316c5b0073cc23e9b21d3d951bc5e4438586797/doc/csvFeed.PNG?token=a3482bf4c492f1445c3fe1f1cd4a9f01ba7a1ec4)
![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/c48e7bbf97c181a92ebd38fa19daeec2441c0eb5/doc/readingCSV.png?token=972f26be628078520d9be830be3f988b3260a60f)
	
- See MainTest example
	
[Checks,Transforms & Storing Parameters](checksTransformsAndStoringParameters.md) |:-:| [Looping & Sessions](loopingAndSessions.md)


