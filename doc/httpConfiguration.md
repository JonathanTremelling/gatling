Gatling Presentation
==================

HTTP Configuration
--------------

- Includes the following basic attributes:
	- **baseURL** - The URL root to point your test at
	- **acceptHeader** - Contains common headers such as "text/html"
	- **doNotTrackHeader**
	- **acceptLanguageHeader**
	- **acceptEncodingHeader**
	- **userAgentHeader**
	
![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/8c191bdaf361df47bd90c093b29a886ae9de3552/doc/httpConfExample.PNG?token=cb00d68e22a21f40041a969601ae7abd05f3c874)
	
- Passed into the Gatling setup command as a protocol

![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/8c191bdaf361df47bd90c093b29a886ae9de3552/doc/httpConfIntegration.PNG?token=dc4045ed1e3a915df48b84f1dab83b3c38288e27)


![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/948d7b8c8563e70067a391cfd8398fa92b4c975b/doc/httpHeaders.PNG?token=860586a6cdf365397c12bb0e7e11cf3b35652bd3)
![alt tag](https://bytebucket.org/JonathanTremelling/gatling/raw/948d7b8c8563e70067a391cfd8398fa92b4c975b/doc/proxy.PNG?token=27de5dd9899b0ae7bd4720ad8d9ed7987acb3475)

    
[Index](presentation.md) |:-:| [Scenarios](scenarios.md)