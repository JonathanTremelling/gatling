Gatling Presentation
==================

Maven & Jenkins
--------------

- To run the test project, specifying a specific test
	- mvn gatling:execute -Dgatling.simulationClass=gatling.BasicLoginTest
- Or simply run all tests
	- mvn gatling:execute


	
Gatling Jenkins Plugin
-----------------------
- https://wiki.jenkins-ci.org/display/JENKINS/Gatling+Plugin

	
[Looping & Sessions](loopingAndSessions.md) |:-:| [Additional Setup Parameters](simulationAdditionalSetup.md)


