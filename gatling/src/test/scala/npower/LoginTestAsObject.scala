package npower

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import java.util.concurrent.ThreadLocalRandom

class LoginTestAsObject extends Simulation {

    val httpConf = http
        .baseURL("http://localhost:8090") // Here is the root for all relative URLs
        .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

	object FullRun {
		var login = exec(http("Login")
			.post("/oauth/token")
			.header("Content-Type", """application/x-www-form-urlencoded""")
			.formParam("""username""", "asuser11")
			.formParam("""password""", "testuser1")
			.formParam("""grant_type""", """password""")
			.formParam("""client_id""", """MobileClient""")
			.check(status.is(200)))
		  .pause(10)
		  
		var login2 = exec(http("Login2")
			.post("/oauth/token")
			.header("Content-Type", """application/x-www-form-urlencoded""")
			.formParam("""username""", "asuser12")
			.formParam("""password""", "testuser1")
			.formParam("""grant_type""", """password""")
			.formParam("""client_id""", """MobileClient""")
			.check(status.is(200)))
		  .pause(10)
	}
	
	object SecondRun {
		var login3 = exec(http("Login3")
			.post("/oauth/token")
			.header("Content-Type", """application/x-www-form-urlencoded""")
			.formParam("""username""", "asuser15")
			.formParam("""password""", "testuser1")
			.formParam("""grant_type""", """password""")
			.formParam("""client_id""", """MobileClient""")
			.check(status.is(200)))
		  .pause(10)
	}
	
	var scn = scenario("Full").exec(
										FullRun.login,
										FullRun.login2,
										SecondRun.login3
									)

    //setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))
	setUp(scn.inject(rampUsers(10) over (10 seconds)).protocols(httpConf))
}
