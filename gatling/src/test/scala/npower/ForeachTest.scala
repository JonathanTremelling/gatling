package npower

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import java.util.concurrent.ThreadLocalRandom

class ForeachTest extends Simulation {

    val httpConf = http
        .baseURL("http://jdtremelling.com") // Here is the root for all relative URLs
        .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

	var scn = scenario("Full").exec(http("Loop")
		.get("/loop.json")
		.header("Content-Type", """application/x-www-form-urlencoded""")
		.check(jsonPath("$.result").is("SUCCESS"),
		  jsonPath("$.data[*]").ofType[Map[String,Any]].findAll.saveAs("pList")))
		.foreach("${pList}", "player") {
		  exec(session => {
		    val playerMap = session("player").as[Map[String,Any]]
		    val playerId = playerMap("playerId")
		    println(playerId)
		    session
		  })
		}
		.exec(session => {
		  // print the Session for debugging, don't do that on real Simulations
		  println(session)
		  session
		})
		.exec(session =>
		  // session.setAttribute returns the new Session, and Scala automatically returns the last assignment
		  // braces are omitted as there's only one instruction
		  session.set("foo", "bar")
		)
		.exec(session => {
		  // print the Session for debugging, don't do that on real Simulations
		  println(session)
		  session
		})
		.exec(session =>
		  // session.setAttribute returns the new Session, and Scala automatically returns the last assignment
		  // braces are omitted as there's only one instruction
		  session.set("foo", "Test")
		)
		.exec(session => {
		  // print the Session for debugging, don't do that on real Simulations
		  println(session)
		  session
		})
	  .pause(10)

    setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))
}
