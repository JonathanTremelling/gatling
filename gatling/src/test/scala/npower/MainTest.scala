package npower

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import java.util.concurrent.ThreadLocalRandom

class MainTest extends Simulation {

    val httpConf = http
        .baseURL("http://localhost:8090") // Here is the root for all relative URLs
        .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
        .doNotTrackHeader("1")
        .acceptLanguageHeader("en-US,en;q=0.5")
        .acceptEncodingHeader("gzip, deflate")
        .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

    val comb_header_1 = Map("Content-Type" -> """application/json""", "Authorization" -> "Bearer${accessToken}", "X-NPOWER-Business-Partner-ID" -> "${businessPartnerID}")

    val userCredentials = csv("stagingUsers.csv").circular

    object FullRun {

        val login = exec(http("Login")
            .post("/oauth/token")
            .header("Content-Type", """application/x-www-form-urlencoded""")
            .formParam("""username""", "${username}")
            .formParam("""password""", "${password}")
            .formParam("""grant_type""", """password""")
            .formParam("""client_id""", """MobileClient""")
            .check(status.is(200))
            .check(jsonPath("$.access_token").saveAs("accessToken"))
            .check(header("x-npower-business-partner-ids")
            .transform(_.split('|')(0))
            .saveAs("businessPartnerID")))
          .pause(10)

        val accounts = exec(http("Accounts")
            .get("/accounts/")
            .headers(comb_header_1)
            .check(jsonPath("$..account_id").saveAs("accountID")))
          .pause(10)

        val transactions = exec(http("Transactions")
            .get("/accounts/${accountID}/transactions")
            .headers(comb_header_1))
          .pause(10)

        val meterReadGroups = exec(http("Meter Read Groups")
            .get("/accounts/${accountID}/meter_read_groups")
            .headers(comb_header_1))
          .pause(10)

        val profile = exec(http("Profiles")
            .get("/accounts/${accountID}/profiles")
            .headers(comb_header_1))
          .pause(10)

        val events = exec(http("Events")
            .get("/accounts/${accountID}/events")
            .headers(comb_header_1))
          .pause(10)

        val consumptionGroups = exec(http("Consumption Groups")
            .get("/accounts/${accountID}/consumption_groups")
            .headers(comb_header_1))
          .pause(10)

        val timelines = exec(http("Timelines")
            .get("/accounts/${accountID}/timelines")
            .headers(comb_header_1))
          .pause(10)
    }

    val runAll = scenario("Full").feed(userCredentials).exec(
                                                              FullRun.login, 
                                                              FullRun.accounts, 
                                                              FullRun.transactions, 
                                                              FullRun.meterReadGroups, 
                                                              FullRun.profile, 
                                                              FullRun.events,
                                                              FullRun.consumptionGroups,
                                                              FullRun.timelines
                                                            )

    setUp(
      runAll.inject(rampUsers(12) over (10 seconds))
    ).protocols(httpConf)
}
