Npower Load Testing With Gatling
=========================

Contains various files for testing different parts of the nPower middleware

```

Start SBT
---------
```bash
$ sbt
```

Run all simulations
-------------------

```bash
> test
```

Run a single simulation
-----------------------

```bash
> testOnly npower.devBox.DevBoxMainTestLive
> testOnly npower.localhost.LocalhostMainTestLive
> testOnly npower.localhost.LocalhostMainTestStaging
```

List all tasks
--------------------

```bash
> tasks
```

User Credentials
----------------
User credentials can be added to the csv files in resources accordingly
>username,password
